blender-flemanco
===========================

Warning: This is currently not working and is kept here as notes for whoever needs some input - or me in the future.

Issues:

* flamenco needs a server.
* the pci passthrough is currently noise


We do pci passthrough, ensure that nouveau driver is not running, and that vfio-pci is set up correctly.

You may need to update the specific passthrough device.

otherwise, to use run `vagrant up`


refs
-----
* https://www.flamenco.io/download/
* https://www.flamenco.io/docs/user_manual/installation/